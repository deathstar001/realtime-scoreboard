const redisQueryUtils = require('../dal/redisUtils');

function getInfo(id, callback) {

    return redisQueryUtils.zscore(id, function (err, score) { // get the current zscore for id
        if (err){
            return callback(err, null);
        }
        // using zrevrangebyscore to find the 1st id with the same zscore
        return redisQueryUtils.zrevrangebyscoreWithOneLimit(parseInt(score),parseInt(score), function (err, resp) {
            if (err)
                return callback(err, null);
            // using zrevrank to find the zrank of the 1st id with same score
            return redisQueryUtils.zrevrank(resp[0], function (err, rank) {
               if (err)
                   return callback(err, null);
                var info = {id:id, score:parseInt(score), rank:rank + 1}; // zranks are zero indexed so adding +1
                return callback(null, info);
            });

        });

    });
}

module.exports = {
    getInfo : getInfo
};