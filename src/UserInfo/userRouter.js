const router = require('express').Router();
const controller = require('./userController');


router.get('/:id', function (request, response) {
    if (!request.params.id){
        return response.status(400).send("User not found!!");
    }

    return controller.getInfo(request.params.id, function (err, data) {
        if (err || !data){
            console.log(err);
            return response.status(404).json({});
        }

        return response.status(200).json(data);
    });
});

module.exports = router;