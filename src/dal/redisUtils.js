const redis = require('redis');
const constant = require('../Constant');
const redisClient = redis.createClient(constant.REDIS_CLIENT_URL);


redisClient.on("ready", function () {
    console.log("Connected to Redis !!");
});

redisClient.on("error", function (err) {
    console.log("Connection to redis failed !! ERROR: " + err);
});

function zadd(key, val, callback) {
    redisClient.zadd(constant.SCORE_BOARD, val, key, callback);
}

function zscore(key, callback) {
    redisClient.zscore(constant.SCORE_BOARD, key, function (err, data) {
        if (err)
            return callback(new Error(err), null);
        if (!data)
            return callback(new Error('User not found !'), null);
        return callback(null, data);
    });
}

function zrevrange(offset, limit, callback) {
    redisClient.zrevrange(constant.SCORE_BOARD, offset, offset+limit-1, 'withscores',function (err, data) {
        if (err || !data)
            return callback(new Error(err), null);
        if (!data)
            return callback(null, []);
        return callback(null, data);
    });
}

function zrevrangebyscoreWithOneLimit(from, to, callback) {
    redisClient.zrevrangebyscore(constant.SCORE_BOARD, from, to, 'limit' , 0, 1,function (err, data) {
        if (err)
            return callback(new Error(err));
        if (!data)
            return callback(new Error("Empty Response from redis"), null);
        return callback(null, data);
    });
}

function zrevrank(key, callback) {
    redisClient.zrevrank(constant.SCORE_BOARD, key,function (err, rank) {
        if (err)
            return callback(new Error(err));
        if (rank === null)
            return callback(new Error("Empty Response from redis"), null);
        return callback(null, rank);
    });
}

module.exports = {
    zadd : zadd,
    zscore : zscore,
    zrevrange : zrevrange,
    zrevrank: zrevrank,
    zrevrangebyscoreWithOneLimit : zrevrangebyscoreWithOneLimit
};