const router = require('express').Router();
const controller = require('./updateController');


router.post('/:id', function (request, response) {
    if (!request.params.id || !request.body || !request.body.score){
        return response.status(400).send("Invalid Request !!");
    }
    var params = {id:request.params.id, score:request.body.score};

    return controller.updateInfo(params, function (err, resp) {
       if (err || !resp){
           console.log(err);
           return response.status(404).send("Updation failed!");
       }

       return response.status(200).send("Updated successfully !!");
    });
});

module.exports = router;