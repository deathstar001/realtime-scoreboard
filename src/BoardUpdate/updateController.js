const redisQueryUtils = require('../dal/redisUtils');

function updateInfo(params, callback) {

    return redisQueryUtils.zadd(params.id, params.score, function (err) {
       if (err){
            return callback(new Error("Redis error"), null);
       }
        return callback(null, "success");
    });
}

module.exports = {
    updateInfo : updateInfo
};