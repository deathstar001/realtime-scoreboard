const redisQueryUtils = require('../dal/redisUtils');

function getFormattedResp(data) {
    var resp  = [];
    for (var i=0;i<data.length;i+=2){
        resp.push({id:data[i],score:parseInt(data[i+1])});
    }

    return resp;
}

function getRankFormattedResp(startRank, data, delta) {
    var resp = [];
    var startScore = data[0].score;
    var count = delta;

    for(var i=0;i<data.length;i++){
        if (data[i].score === startScore){ // as long as the scores are similar incr the count and assign the same rank
            data[i].rank = startRank;
            resp.push(data[i]);
            count += 1;
        }
        else{
            startRank += count; // add this count delta to incr the rank of next ids.
            count=0;
            startScore = data[i].score;
            i--;
        }
    }
    return resp;
}

function getBatch(params, callback) {

    // using zrevrange to find all the elements from offset to offset + limit -1
    return redisQueryUtils.zrevrange(params.offset, params.limit, function (err, data) {
        if (err){
            return callback(err, null);
        }
        if (!data || data.length === 0)
            return callback(null, []);

        var resp = getFormattedResp(data);

        // getting the rank of the 1st element from offset query
        return redisQueryUtils.zrevrank(resp[0].id, function (err, rank1) {
            if (err)
                return callback(err, null);
            // using zrevrangebyscore to get the 1st id with score = score of 1st element in offset
            return redisQueryUtils.zrevrangebyscoreWithOneLimit(resp[0].score,resp[0].score, function (err, elem) {
                if (err)
                    return callback(err, null);
                // getting the rank of the 1st id with same score
                return redisQueryUtils.zrevrank(elem[0], function (err, rank2) {
                    if (err)
                        return callback(err, null);
                    // using the 2 ranks to compute delta i.e the no. of elements with same score = 1st element in offset
                    return callback(null, getRankFormattedResp(rank2+1, resp, rank1-rank2));
                });

            });
        });
    });
}

module.exports = {
    getBatch : getBatch
};