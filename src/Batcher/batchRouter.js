const router = require('express').Router();
const controller = require('./batchController');


router.get('/', function (request, response) {
    if (!request.query.offset || !request.query.limit || request.query.limit < 1){
        return response.status(400).send("Invalid Request !!");
    }
    try {
        var params = {offset: parseInt(request.query.offset), limit: parseInt(request.query.limit)};
    } catch (err){
        return response.status(400).send("Invalid Request !!");
    }

    return controller.getBatch(params, function (err, data) {
        if (err || !data){
            console.log(err);
            return response.status(404).send("Error getting batch !!");
        }

        return response.status(200).json(data);
    });
});

module.exports = router;