const express = require('express');
const bodyParser = require('body-parser');

const app = express();

app.use(bodyParser.json({limit: '10mb'}));
app.use(bodyParser.urlencoded({
    extended: true,
    limit: '10mb'
}));

app.get('/ping', function (req, res) {
    return res.send('PONG');
});

app.use(['/updatescore'], require('./src/BoardUpdate/updateRouter'));
app.use(['/getuserinfo'], require('./src/UserInfo/userRouter'));
app.use(['/getbatch'], require('./src/Batcher/batchRouter'));

app.set('port', 3000);

app.listen(app.get('port'), function () {
    console.log("App Started  : " + process.pid);
    console.log("Listening on port : " + app.get('port'));
});



